function proba = distpoi(mean,n) 
    proba = zeros(n+1,1) ;
    for k = 0:1:n
        proba(k+1) = exp(k*log(mean) - mean - gammaln(k+1)) ;
        if k == 0
            proba(k+1,2) = proba(k+1,1) ;
        else
            proba(k+1,2) = proba(k,2) + proba(k+1,1) ;
        end
    end
end