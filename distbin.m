function proba = distbin(mean,n) 
    p = mean/n ;
    proba = zeros(n+1,2) ;
    for k = 0:1:n
        proba(k+1,1) = exp(gammaln(n+1) - gammaln(k+1) - gammaln(n-k+1) + k * log(p) + (n-k) * log(1-p)) ;
        if k == 0
            proba(k+1,2) = proba(k+1,1) ;
        else
            proba(k+1,2) = proba(k,2) + proba(k+1,1) ;
        end
    end
end